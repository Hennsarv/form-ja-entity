﻿namespace FormJaEntity
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.TooteNupp = new System.Windows.Forms.Button();
            this.KategooriaNupp = new System.Windows.Forms.Button();
            this.SaveNupp = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(67, 56);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(675, 305);
            this.dataGridView1.TabIndex = 0;
            // 
            // TooteNupp
            // 
            this.TooteNupp.Location = new System.Drawing.Point(67, 368);
            this.TooteNupp.Name = "TooteNupp";
            this.TooteNupp.Size = new System.Drawing.Size(75, 23);
            this.TooteNupp.TabIndex = 1;
            this.TooteNupp.Text = "Tooted";
            this.TooteNupp.UseVisualStyleBackColor = true;
            this.TooteNupp.Click += new System.EventHandler(this.TooteNupp_Click);
            // 
            // KategooriaNupp
            // 
            this.KategooriaNupp.Location = new System.Drawing.Point(269, 367);
            this.KategooriaNupp.Name = "KategooriaNupp";
            this.KategooriaNupp.Size = new System.Drawing.Size(75, 23);
            this.KategooriaNupp.TabIndex = 1;
            this.KategooriaNupp.Text = "Kategooriad";
            this.KategooriaNupp.UseVisualStyleBackColor = true;
            this.KategooriaNupp.Click += new System.EventHandler(this.KategooriaNupp_Click);
            // 
            // SaveNupp
            // 
            this.SaveNupp.Location = new System.Drawing.Point(667, 368);
            this.SaveNupp.Name = "SaveNupp";
            this.SaveNupp.Size = new System.Drawing.Size(75, 23);
            this.SaveNupp.TabIndex = 1;
            this.SaveNupp.Text = "Save";
            this.SaveNupp.UseVisualStyleBackColor = true;
            this.SaveNupp.Click += new System.EventHandler(this.SaveNupp_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(67, 22);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(797, 414);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.SaveNupp);
            this.Controls.Add(this.KategooriaNupp);
            this.Controls.Add(this.TooteNupp);
            this.Controls.Add(this.dataGridView1);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button TooteNupp;
        private System.Windows.Forms.Button KategooriaNupp;
        private System.Windows.Forms.Button SaveNupp;
        private System.Windows.Forms.Button button1;
    }
}

