﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FormJaEntity
{
    public partial class Form1 : Form
    {
        static NorthwindEntities ne = new NorthwindEntities();
        public Form1()
        {
            InitializeComponent();
        }

        private void TooteNupp_Click(object sender, EventArgs e)
        {
            this.dataGridView1.DataSource = ne.Products.ToList();
        }

        private void KategooriaNupp_Click(object sender, EventArgs e)
        {
            this.dataGridView1.DataSource = ne.Categories.ToList();

        }

        private void SaveNupp_Click(object sender, EventArgs e)
        {
            ne.SaveChanges();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form2 teine = new Form2();
            teine.Show();
        }
    }
}
