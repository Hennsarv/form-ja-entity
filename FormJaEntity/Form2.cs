﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FormJaEntity
{
    public partial class Form2 : Form
    {

        NorthwindEntities ne = new NorthwindEntities();

        public Form2()
        {
            InitializeComponent();
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            this.comboBox1.DataSource = ne.Categories.Select(x => x.CategoryName).ToList();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.dataGridView1.DataSource =
                ne.Categories.Where(
                    c => c.CategoryName == this.comboBox1.SelectedItem.ToString())
                    .Single().Products.ToList();

        }
    }
}
